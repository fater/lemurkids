<?php
require_once '_header.php';
?>

    <div id="carousel-content-row-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carousel-content-row-generic" class="active" data-slide-to="1"></li>
            <li data-target="#carousel-content-row-generic" class="active-none" data-slide-to="2"></li>
            <li data-target="#carousel-content-row-generic" class="active-none-none" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <a href="index.html"><img
                        src="http://lemur.kislorod2.ru/admin/upload/0a552673b97a1bd4a702ab733ac99e68.jpg">
                </a>
            </div>
            <div class="item active-none">
                <a href="index.html"><img src="admin/upload/b76f246aab8a1d2c261c8a7ffe08df77.jpg"> </a>
            </div>
            <div class="item active-none-none">
                <a href="index.html"><img src="admin/upload/d0a4a93a9fb5315a00dde01f8bb965da.jpg"> </a>
            </div>

        </div>
        <a class="left carousel-control" href="index.html#carousel-content-row-generic" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="index.html#carousel-content-row-generic" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>

    <br>

    <div class="lemur-cup">
        <div class="lemur-cup-left">
            <div class="lemur-cup-cup"><i class="fa fa-trophy fa-4"></i>
            </div>
            <div class="lemur-cup-month">
                Рейтинг <br>
                <span class="lemur-cup-month">[Апрель]</span>
            </div>
        </div>
        <div class="lemur-cup-right">
            <div class="lemur-rating col-md-4">
                <div class="lemur-rating-item w-100 m-10">
                    1.
                    Dastanbek <span class="badge badge-primary lemur-bage float-right"> 135</span>
                </div>
                <div class="lemur-rating-item w-100 m-10">
                    2.
                    Vladislav <span class="badge badge-primary lemur-bage float-right"> 105</span>
                </div>
                <div class="lemur-rating-item w-100 m-10">
                    3.
                    Lemur <span class="badge badge-primary lemur-bage float-right"> 100</span>
                </div>
                <div class="lemur-rating-item w-100 m-10">
                    4.
                    Веб-студия Kislorod <span class="badge badge-primary lemur-bage float-right"> 100</span>
                </div>
            </div>
        </div>
    </div>
    <div class="quizs">

        <div class="panel lemur-item  ">
            <div class="row">
                <div class="col-md-3">
                    <div class="jumbotron">
                        <div class="jumbotron-photo">
                            <img class="img-thumbnail" width="304" height="236" src="upload/image/quiz_8.jpeg">
                        </div>
                    </div>

                </div>
                <div class="col-md-9">
                    <h3 class="lemur-month">
                        <span class="lemur-month-md">11 май</span>
                        </br><span class="lemur-item-year">2016</span></h3>
                    <h2 class="lemur-item-title">Мадагаскар 3</h2>
                    <div class="content" style="max-height: 3em; overflow: hidden;">
                        <p>
                            Лев Алекс, зебра Марти, гиппопотамиха Глория и жираф Мелман, а также король Джулиан,
                            Морис, Морт и Пингвины все еще пытаются вернуться в Нью-Йорк. На этот раз их путь
                            пройдет через Европу, где они откроют сво
                        </p></div>
                    <div class="row bg-white lemur-item-badges">
                        <div class="col-md-4 lemur-item-badges-item"><span
                                class="badge badge-primary float-right">6</span>Вопросов
                        </div>
                        <div class="col-md-4 lemur-item-badges-item"><span
                                class="badge badge-success float-right">30</span>Максимум
                            баллов
                        </div>
                        <div class="col-md-4 lemur-item-badges-item"><span
                                class="badge badge-warning float-right">52</span>Прошли
                            опрос
                        </div>
                    </div>
                </div>

            </div>
            <a href="quiz-info.php-quiz=8.html" class="btn btn-primary lemur-item-read-more">Подробнее...</a>
        </div>
        <a href="http://lemur.kislorod2.ru/quiz.php" class="btn-block btn-default m-t-10 m-b-20 text-center btn-lg">Все
            опросы</a>
    </div>
<?php
require_once '_footer.php';
?>
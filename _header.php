<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Lemur KIDS</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="admin/upload/72661b9df5e99e54434d2455c3a7f608.ico"/>
    <meta name='yandex-verification' content='6f3a5cc1ae712cf4'/>
    <link rel="bookmark"
          href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic"/>
    <!-- site css -->
    <!--    <link rel="stylesheet" href="http://lemur.kislorod2.ru/dist/css/site.min.css">-->
    <link href="https://fonts.googleapis.com/css?family=Marmelad&subset=cyrillic" rel="stylesheet">
    <!--    <link rel="stylesheet" href="/dist/css/site.min.css">-->
    <link rel="stylesheet" href="/dist/css/bootstrap.3.3.7.min.css">
    <link rel="stylesheet" href="/dist/css/new.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!--     <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>!important
    <script src="http://lemur.kislorod2.ru/js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="http://lemur.kislorod2.ru/dist/js/site.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <a href="index.php"><img src="admin/upload/e109c32b357f27e458a8a0d1068211e2.png" alt="" ></a>
        </div>
        <div class="col-sm-4">
            <br>
            <br>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Строка поиска">
                <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
                </span>
            </div>
        </div>
        <div class="col-sm-4 text-right">
            <br>
            <a class="float-right" href="youtube.php.html"><img src="upload/image/youtube.png" alt="" class="lemur-logo-youtube"></a>

            <div class="btn-group">
                <a href="google.php.html" class="btn btn-md text-right btn-primary">Войти</a>
                <a href="google.php.html" class="btn btn-md media-right btn-info">Регистрация</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="main-menu">
            <a href="/" class="btn btn-md lemur-menu-item ">Главная</a>
            <a href="lemur_kids.php.html" class="btn btn-md lemur-menu-item ">О проекте</a>
            <a href="http://lemur.kislorod2.ru/quiz.php" class="btn btn-md lemur-menu-item ">Опросы</a>
            <a href="http://lemur.kislorod2.ru/rating.php" class="btn btn-md lemur-menu-item ">Награда</a>
            <a href="youtube.php.html" class="btn btn-md lemur-menu-item ">Наш канал</a>
        </div>
    </div>
    <br>
